package com.example.demoApiOne.controller;

import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demoApiOne.entity.ContractEntity;
import com.example.demoApiOne.service.ContractService;
import java.sql.Date;
import org.springframework.web.bind.annotation.PutMapping;

/**
 *
 * @Info: Type: Class, Name: Request, Fecha-creada:06/02/2019
 * NOTA: @Clase donde entrarán los datos de F.U. en {JSON}
 * 
 * forma Json POST...:
 * {
        "numContrato":"17",
        "solicitudPadre":"FU14",
        "solicitudHija":"LE13",
        "tasa":"18",
        "estado":"IN",
        "usuario":"ANTAPIAV",
        "fecha":"2019-02-06T15:11:00.000-05:00"
    }
 */
@RestController
@RequestMapping("/v1")
public class Request {
    private static final Log logger = LogFactory.getLog(Request.class);
    
    @Autowired
    @Qualifier("contractService")
    ContractService contractServices;
    
    /*Metodo de prueba para pasar cada dato de Json a la clase contractEntity*/
    @PostMapping("/request")
    public String obtener(@RequestBody @Valid ContractEntity contractEntity){
        logger.info("(1)CLASS: Request, Method: highContract, status:ok");
        System.out.println(contractEntity.getFecha());
        //contractEntity = new ContractEntity();
        //contractEntity.setFecha(Date.valueOf(contractEntity.getFecha().getYear()+"-"+contractEntity.getFecha().getMonth()+"-"+contractEntity.getFecha().getDay()));
        //System.out.println(Date.valueOf(contractEntity.getFecha().getYear()+"-"+contractEntity.getFecha().getMonth()+"-"+contractEntity.getFecha().getDay()));
        //return contractServices.obtenerNumContrato(contractEntity);
        //return contractServices.altaContrato(contractEntity);
        return contractEntity.getNumContrato().toString();
    } 
    
    /*Metodo de prueba para registrar datos en BD T_refiero --- devuelve un true si es correcto*/
    @PostMapping("/altaSolicitud")
    public boolean altaContrato(@RequestBody @Valid ContractEntity contractEntity){
        if(contractEntity.getNumContrato() == null){
            logger.info("(2)CLASS: Request, Method: highContract, status:ok");
        }else{
            logger.info("(2.1)CLASS: Request, Method: highContract, status:Existe numero de contrato");
        }
        return contractServices.altaContrato(contractEntity);
    } 
    @PutMapping("/altaSolicitud")
    public boolean actualizarContrato(@RequestBody @Valid ContractEntity contractEntity){
        if(contractEntity.getNumContrato() != null){
            logger.info("(3)CLASS: Request, Method: highContract, status:actualización ok");
        }else{
            logger.info("(3.1)CLASS: Request, Method: highContract, status: llegó nulo");
        }
        return contractServices.actualizarContrato(contractEntity);
    } 
}
