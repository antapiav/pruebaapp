package com.example.demoApiOne.repository;

import com.example.demoApiOne.entity.ContractEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @Info: Type: Interface, Name: ContractRepository, Fecha-creada:06/02/2019
 * NOTA: posiblememte se imlemente posteriormente en persistencia para BD T-refiero 
 * Por ahora sin implementar
 */
@Repository("contractRepository")
public interface ContractRepository extends JpaRepository<ContractEntity, Serializable> {
    public abstract ContractEntity findByIdAndSolicitudPadreAndSolicitudHija (long id, long solicitudPadre, long solicitudHija);
    /*public abstract ContractEntity findById(long id);
    public abstract List<ContractEntity> findBySolicitudPadre (long solicitudPadre);
    public abstract List<ContractEntity> findBySolicitudHija (long solicitudHija);*/
}
