package com.example.demoApiOne.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @clase ContractEntity
 */
@Entity
@Table(name="PRUEBA")
public class ContractEntity implements Serializable {
    
    @GeneratedValue
    @Id
    @Column(name="ID",unique = true)
    private long id;

    @Column(name="NUM_CONTRATO",unique = true)
    private String numContrato;
    
    @Column(name="SOLICITUD_PADRE",unique = true)
    private String solicitudPadre;

    @Column(name="SOLICITUD_HIJA")//,unique = true)
    private String solicitudHija;

    @Column(name="TASA")
    private int tasa;
    
    @Column(name="ESTADO")
    private String estado;

    @Column(name="USUARIO")
    private String usuario;
    
    @Column(name="FECHA")
    private Date fecha;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(String numContrato) {
        this.numContrato = numContrato;
    }

    public String getSolicitudPadre() {
        return solicitudPadre;
    }

    public void setSolicitudPadre(String solicitudPadre) {
        this.solicitudPadre = solicitudPadre;
    }

    public String getSolicitudHija() {
        return solicitudHija;
    }

    public void setSolicitudHija(String solicitudHija) {
        this.solicitudHija = solicitudHija;
    }

    public int getTasa() {
        return tasa;
    }

    public void setTasa(int tasa) {
        this.tasa = tasa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
