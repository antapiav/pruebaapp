package com.example.demoApiOne.converter;

import com.example.demoApiOne.entity.ContractEntity;
import com.example.demoApiOne.model.ContractModel;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author pasar lista de consulta Hibernet a Clase contractModel y evitar error
 * NOTA: No necesaria sin uso solo para perueba...
 */
@Component("convertidor")
public class ConverterList {
    public List<ContractModel> convertirLista(List<ContractEntity> contractEntitys){
        List<ContractModel> contractModel = new ArrayList<>();
        for (ContractEntity contractEntity : contractEntitys){
            contractModel.add(new ContractModel(contractEntity));
        }
        return contractModel;
    }
}
