package com.example.demoApiOne.service;


import com.example.demoApiOne.entity.ContractEntity;
import com.example.demoApiOne.repository.ContractRepository;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @Info: Type: Class, Name: ContractService, Fecha-creada:06/02/2019
 * NOTA: @Clase donde entrarán los datos de F.U. en {JSON}
 */
@Service("contractService")
public class ContractService {
    
    private static final Log logger = LogFactory.getLog(ContractService.class);
    
    @Autowired
    @Qualifier("contractRepository")
    private ContractRepository contractRepository;
    
    /*@Autowired
    @Qualifier("convertidor")
    private ConverterList converterList;
    public List<ContractModel> obtenNumContrato(long solicitudPadre, long){
        return converterList.convertirLista(contractRepository.findBySolicitudPadre(solicitudPadre));/////////////////////
    }*/
    
      /********************/
     /*Obtener N°contrato*/
    /********************/
    public boolean obtenerNumContrato(ContractEntity contractEntity){
        logger.info("(1)CLASS: ContractService, METHOD: obtenerNumContrato");
        try{
            contractRepository.save(contractEntity);
            //return new ContractModel(contractRepository.findByIdAndSolicitudPadreAndSolicitudHija(id, solicitudPadre, solicitudHija));
            logger.info("(2)CLASS: ContractService, METHOD: obtenerNumContrato, OK");
            return true;
        }catch(Exception e){
            logger.error("(3)CLASS: ContractService, METHOD: obtenerNumContrato, ERROR: "+e);
            return false;
        }
    }
    
      /***************************************/
     /*Guardar en base de datos de T-Refeiro*/
    /***************************************/
    public boolean altaContrato(ContractEntity contractEntity){
        logger.info("(4)CLASS: ContractService, METHOD: altaContrato");
        try{
            contractRepository.save(contractEntity);///////////////////////
            logger.info("(4.1)CLASS: ContractService, METHOD: altaContrato, Registró: ok");
            return true;
        }catch(Exception e){
            logger.error("(4.2)CLASS: ContractService, METHOD: altaContrato, ERROR: "+e);
            return false;
        }
    }
     
    /*Actualizar.. duda existencial si lso de HOST :( */
    public boolean actualizarContrato(ContractEntity contractEntity){
        logger.info("(5)CLASS: ContractService, METHOD: actualizarContrato");
        try{
            contractRepository.save(contractEntity);///////////////////////
            logger.info("(5.1)CLASS: ContractService, METHOD: altaContrato, Actualizó: ok");
            return true;
        }catch(Exception e){
            logger.error("(5.2)CLASS: ContractService, METHOD: altaContrato, ERROR: "+e);
            return false;
        }
    }
    
}
