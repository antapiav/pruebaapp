package com.example.demoApiOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApiOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApiOneApplication.class, args);
	}

}

