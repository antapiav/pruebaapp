package com.example.demoApiOne.model;

import com.example.demoApiOne.entity.ContractEntity;
import java.util.Date;

/**
 *
 * @Clase ContractModel
 */
public class ContractModel {
    
    private long id;
    private String numContrato;
    private String solicitudPadre;
    private String solicitudHija;
    private int tasa;
    private String estado;
    private String usuario;
    private Date fecha;
    
    public ContractModel() {
    }
    
    public ContractModel(ContractEntity contractEntity) {
        this.id = contractEntity.getId();
        this.numContrato = contractEntity.getNumContrato();
        this.solicitudPadre = contractEntity.getSolicitudPadre();
        this.solicitudHija = contractEntity.getSolicitudHija();
        this.tasa = contractEntity.getTasa();
        this.estado = contractEntity.getUsuario();
        this.usuario = contractEntity.getUsuario();
        this.fecha = contractEntity.getFecha();
    }

    public ContractModel(long id, String numContrato, String solicitudPadre, String solicitudHija, int tasa, String estado, String usuario, Date fecha) {
        this.id = id;
        this.numContrato = numContrato;
        this.solicitudPadre = solicitudPadre;
        this.solicitudHija = solicitudHija;
        this.tasa = tasa;
        this.estado = estado;
        this.usuario = usuario;
        this.fecha = fecha;
    }    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(String numContrato) {
        this.numContrato = numContrato;
    }

    public String getSolicitudPadre() {
        return solicitudPadre;
    }

    public void setSolicitudPadre(String solicitudPadre) {
        this.solicitudPadre = solicitudPadre;
    }

    public String getSolicitudHija() {
        return solicitudHija;
    }

    public void setSolicitudHija(String solicitudHija) {
        this.solicitudHija = solicitudHija;
    }

    public int getTasa() {
        return tasa;
    }

    public void setTasa(int tasa) {
        this.tasa = tasa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
